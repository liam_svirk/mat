﻿using UnityEngine;

public static class LoadingManager
{
    private static LevelData levelData;

    public static LevelData LoadForPlay(int currentLevel, LevelData levelData)
    {
        levelData = new LevelData(Application.isPlaying, currentLevel);
        return LoadFromScriptable(currentLevel, levelData);
    }

    private static LevelData LoadFromScriptable(int currentLevel, LevelData levelData)
    {
        var levelScriptable = PreloaderLevel.LevelScriptable;
        if (levelScriptable == null) levelScriptable = Resources.Load("Levels/LevelScriptable") as LevelScriptable;
        var _levelScriptable = levelScriptable;
        var ld = _levelScriptable.levels.TryGetElement(currentLevel - 1, null);
        levelData = ld.DeepCopyForPlay(currentLevel);
        LevelData.THIS = levelData;
        levelData.LoadTargetObject();
        levelData.InitTargetObjects();

        return levelData;
    }
    
    public static LevelData LoadFromScriptableForEditor(int currentLevel, LevelData levelData)
    {
        var levelScriptable = Resources.Load("Levels/LevelScriptable") as LevelScriptable;
        var ld = levelScriptable.levels.TryGetElement(currentLevel - 1, null);
        levelData = ld.DeepCopy(currentLevel);
        LevelData.THIS = levelData;
        levelData.LoadTargetObject();
        levelData.InitTargetObjects();

        return levelData;
    }
}

