using System;
using System.Collections.Generic;
using UnityEngine;

public class LevelScriptable : ScriptableObject
{
    public List<LevelData> levels = new List<LevelData>();

    [Serializable]
    public class LevelKeeper
    {
        public int target;
        public int limitType;
        public int limit;
        public int colorLimit;
        public int[] stars = new int[3];
        public bool marmalade;
        public string collectCount;
        public List<FieldData> fields = new List<FieldData>();

    }
}