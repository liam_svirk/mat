using System.Collections;
using UnityEngine;

    public class PreloaderLevel : MonoBehaviour
    {
        public static LevelScriptable LevelScriptable;
        private static PreloaderLevel THIS;
        private void Awake()
        {
            if (THIS == null)
                THIS = this;
            else if(THIS != this)
                Destroy(gameObject);
            DontDestroyOnLoad(this);

            StartCoroutine(Loading());
        }

        IEnumerator Loading()
        {
            var xx = Resources.LoadAsync("Levels/LevelScriptable");
            yield return new WaitUntil(()=>xx.isDone);
            LevelScriptable = xx.asset as LevelScriptable;

        }

    }
