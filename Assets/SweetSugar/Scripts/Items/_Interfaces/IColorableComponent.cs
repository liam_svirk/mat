﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[ExecuteAlways]
public class IColorableComponent : MonoBehaviour/* , IPoolable */
{
    public int color;

    public Sprite[] Sprites;

    public Sprite randomEditorSprite;
    public SpriteRenderer directSpriteRenderer;
    // public IHighlightableComponent IHighlightableComponent;
    // public IDestroyableComponent IDestroyableComponent;
    // public ItemSound ItemSound;
    private void OnEnable()
    {
        if (GetComponent<Item>() && !GetComponent<Item>().Combinable) color = GetHashCode();
    }

    // [HideInInspector]
    // public bool colorGenerated;
    public bool RandomColorOnAwake = true;
    public void SetColor(int _color)
    {
        if (_color < 0 || _color >= Sprites.Length) return;
        // colorGenerated = true;
        var component = GetComponent<Item>();
        if (component != null && component.currentType != ItemsTypes.MULTICOLOR) 
            color = _color;
        if (directSpriteRenderer == null)
            directSpriteRenderer = GetComponentInChildren<SpriteRenderer>();
        if (Sprites.Length > 0 && directSpriteRenderer)
            directSpriteRenderer.sprite = Sprites[_color];
        GetComponentsInChildren<IColorChangable>().ToList().ForEach(i => i.OnColorChanged(_color));
    }

    public void RandomizeColor()
    {
        color = ColorGenerator.GenColor(GetComponent<Item>().square);
        SetColor(color);
        GetComponentsInChildren<IColorableComponent>().ToList().ForEach(i => i.SetColor(color));

    }

}
