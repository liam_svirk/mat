﻿using UnityEngine;

public class DebugSettings : ScriptableObject
{
    public bool BonusCombinesShowLog;
    public bool DestroyLog;
    public bool FallingLog;
    public bool AI;
    [Range(0, 100)]
    public float TimeScaleUI = 1;
    [Range(0, 100)]
    public float TimeScaleItems = 1;

    public bool ShowLogImmediately;
}
