using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SweetSugar.Scripts.System
{
    public class ServerTime : UnityEngine.MonoBehaviour
    {
        public static ServerTime THIS;
        public DateTime serverTime;
        public bool dateReceived;
        public delegate void DateReceived();
        public static event DateReceived OnDateReceived;

        private void Awake()
        {
            THIS = this;
            GetServerTime();
        }

        private void OnEnable()
        {
            GetServerTime();
        }

        void GetServerTime ()
        {
            StartCoroutine(getTime());

        }
  
        IEnumerator getTime()
        {
            WWW www = new WWW("http://85.119.150.22/gettime.php");
            yield return www;
#if UNITY_WEBGL
            serverTime = DateTime.Now;
            #else
                if(www.text != "")
                    serverTime = DateTime.Parse(www.text);
                else
                    serverTime = DateTime.Now;
            #endif
            dateReceived = true;
            OnDateReceived?.Invoke();
        }
    }
}