using System;
using UnityEngine;

namespace SweetSugar.Scripts.System
{
    public class MenuReference : UnityEngine.MonoBehaviour
    {
        public static MenuReference THIS;
        public GameObject PrePlay;
        public GameObject PreCompleteBanner;
        public GameObject MenuPlay;
        public GameObject MenuComplete;
        public GameObject MenuFailed;
        public GameObject PreFailed;
        public GameObject BonusSpin;
        public GameObject BoostShop;
        public GameObject LiveShop;
        public GameObject GemsShop;
        public GameObject Reward;
        public GameObject Daily;
        public GameObject Tutorials;
        public GameObject Settings;
        private void Awake()
        {
            THIS = this;
        }
    }
}