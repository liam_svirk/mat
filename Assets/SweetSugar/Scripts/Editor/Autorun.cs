﻿using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;

[InitializeOnLoad]
public class Autorun
{
    static Autorun()
    {
        EditorApplication.update += InitProject;

    }

    static void InitProject()
    {
        EditorApplication.update -= InitProject;
        if (EditorApplication.timeSinceStartup < 10 || !EditorPrefs.GetBool("AlreadyOpened"))
        {
            if (EditorSceneManager.GetActiveScene().name != "game" && Directory.Exists("Assets/SweetSugar/Scenes"))
            {
                EditorSceneManager.OpenScene("Assets/SweetSugar/Scenes/game.unity");

            }
            LevelMakerEditor.Init();
            LevelMakerEditor.ShowHelp();
            EditorPrefs.SetBool("AlreadyOpened", true);
        }

    }
}