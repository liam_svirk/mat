using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ItemDebugInspector))]
public class ItemDebugInspectorEditor : Editor
{
    private string log;
    private string logDesrt;

    public override void OnInspectorGUI()
    {
        if (GUILayout.Button("Print falling log"))
        {
            DebugLogKeeper.GetLog(((ItemDebugInspector)target).name, DebugLogKeeper.LogType.Falling);
        }
        if (GUILayout.Button("Print destroying log"))
        {
            DebugLogKeeper.GetLog(((ItemDebugInspector)target).name, DebugLogKeeper.LogType.Destroying);
        }
        if (GUILayout.Button("Print bonus log"))
        {
            DebugLogKeeper.GetLog(((ItemDebugInspector)target).name, DebugLogKeeper.LogType.BonusAppearance);
        }
        base.OnInspectorGUI();

    }

}